<?php

namespace AppBundle\Controller;

use Exception;
use ZipArchive;
use Goutte\Client;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;
use Monolog\Logger;
use Monolog\Handler\StreamHandler;
use Monolog\Formatter\LineFormatter;

class ScrapingToolController extends Controller {

    public $emailPattern = "";
    public $baseUrl = "";
    public $requestDelay = "";
    public $therapistNameCssSelector = "";
    public $therapistProfileCssSelector = "";
    public $amountOfPages = "";
    public $proxy = "";
    public $amountOfProxies = "";
    public $useBaseIp = "";
    public $logger;
    public $therapistNames = array();
    public $therapistProfileUrls = array();
    public $therapistWebsiteUrls = array();
    public $therapistAddresses = array();
    public $allEmails = array();

    /**
     * ScrapingToolController constructor.
     *
     * @param $base_url
     *   URL on which the whole process starts.
     * @param $delay
     *   Requests delay.
     * @param $amount
     *   Amount of pages to scan additionally.
     */
    public function __construct($rootDir, $base_url = NULL, $delay = NULL, $amount = NULL, $proxyAmount = NULL, $useBaseIp = TRUE) {
        $this->emailPattern = "/^(.*|\s*)[_a-z0-9-]+(\.[_a-z0-9-]+)*@[a-z0-9-]+(\.[a-z0-9-]+)*(\.[a-z]{2,3})(.*|\s*)$/";
        $this->baseUrl = $base_url;
        $this->requestDelay = $delay;
        $this->amountOfPages = $amount;
        $this->amountOfProxies = $proxyAmount;
        $this->useBaseIp = $proxyAmount;
        $this->therapistNameCssSelector = "div.hidden-xs.col-sm-6.col-md-7.col-lg-7 h1";
        $this->therapistProfileCssSelector = "a.result-name";
        $this->logger = new Logger('scraping-tool');

        $dateFormat = "[Y-n-j H:i:s]";
        $output = "%datetime% > %message%\n";
        $formatter = new LineFormatter($output, $dateFormat);
        $stream = new StreamHandler($rootDir . '/scraper.log', Logger::DEBUG);
        $stream->setFormatter($formatter);
        $this->logger->pushHandler($stream);
    }

    /*
     * Main route function.
     *
     * @Route("/", name="homepage")
     */
    public function mainFormAction() {
        if (isset($_POST["url"])) { // If URL given.
            $this->logger->addInfo('Starting the script...');
            // Gets POST parameters.
            $this->baseUrl = $_POST["url"];
            $this->requestDelay = $_POST["delay"];
            $this->amountOfPages = $_POST["amount"];
            $this->amountOfProxies = $_POST["proxies"];
            $this->useBaseIp = $_POST["baseIp"];

            // Clearing CSV file.
            $this->logger->addInfo('Clearing CSV file...');
            $file = fopen('emails.csv', 'w');
            fwrite($file, '');
            fclose($file);

            $this->logger->addInfo('Calling main function...');
            $main = $this->main();
            if ($main != NULL) {
                $this->logger->addInfo('Printing results...');
                $this->printResults();
                $message = "CSV file has been created.";
                $response = $this->printMessage($message);
                return $response;
            } else {
                $message = "There are no results for that page.\nPlease check that you can access that page from your computer.\nAlso check that there are links to therapist's profiles from https://therapists.psychologytoday.com/ on the page.";
                return $this->printMessage($message);
            }
        } elseif (isset($_FILES['file-upload']['name'])) {
            if (isset($_POST["delay"]) && isset($_POST["proxies"])) {
                $this->requestDelay = $_POST["delay"];
                $this->amountOfProxies = $_POST["proxies"];
                $this->useBaseIp = $_POST["baseIp"];
            }

            // Clearing CSV file.
            $this->logger->addInfo('Clearing CSV file...');
            $file = fopen('emails.csv', 'w');
            fwrite($file, '');
            fclose($file);

            $this->logger->addInfo('Getting links from the file...');
            $links = $this->getLinksFromFile($_FILES['file-upload']['tmp_name']);

            // If there are any links - send them to main function of the scraper,
            // print the results and write them to the CSV file.
            if (!empty($links)) {
                $this->logger->addInfo('Calling main function...');
                $main = $this->main($links);
                if ($main != NULL) {
                    $this->logger->addInfo('Printing results...');
                    $this->printResults();
                    $this->logger->addInfo('Creating CSV...');
                    $message = "CSV file has been created.";
                    $response = $this->printMessage($message);
                    return $response;
                } else {
                    // If there are no results for that file.
                    $message = "There are no results for that file.\nPlease check that there are links to therapist's profiles from http://www.aamft.org/ or from https://therapists.psychologytoday.com/ in the file.";
                    return $this->printMessage($message);
                }
            } else {
                // If there are no results for that file.
                $message = "There are no results for that file.\nPlease check that there are links to therapist's profiles from http://www.aamft.org/ or from https://therapists.psychologytoday.com/ in the file.";
                return $this->printMessage($message);
            }
        } else {
            return $this->render("Main/main-form.html.twig");
        }
    }

    public function getLinksFromFile($file) {
        $tempDir = '/tmp/scraping-tables/' . $file;
        $zip = new ZipArchive();
        $zip->open($file);
        $zip->extractTo($tempDir);

        $strings = simplexml_load_file($tempDir . '/xl/sharedStrings.xml');

        $text_strings = array();
        foreach ($strings as $string) {
            $text_strings[] = (string) $string->t;
        }

        $result_strings = array();
        foreach ($text_strings as $item) {
            if (strstr($item, 'http://www.aamft.org/') || strstr($item, 'https://therapists.psychologytoday.com/')) {
                $result_strings[] = $item;
            }
        }

        return $result_strings;
    }

    /**
     * Extract HTML from given site.
     *
     * @param $crawler
     *   Crawler object.
     *
     * @return string
     *   String that contains site's HTML.
     */
    public function getSitesHtml($crawler) {
        if (!empty($crawler)) {
            try {
                // If there are results then get sites HTML.
                $nodes_count = $crawler->count();
                if ($nodes_count > 0) {
                    $result = $crawler->html();
                } else {
                    $result = NULL;
                }
            } catch (Exception $e) {
                $this->logger->addInfo('Exception caught with message: ' . $e->getMessage());
            }
        } else {
            $result = NULL;
        }

        return $result;
    }

    /**
     * Extract therapists name.
     *
     * @param $crawler
     *   Crawler object.
     *
     * @return string
     *   String that contains therapists names.
     */
    public function getTherapistsName($crawler) {
        $therapistName = $crawler->filter($this->therapistNameCssSelector)->extract(array("_text"));
        $therapistName = trim($therapistName[0]);

        return $therapistName;
    }

    /**
     * Extract therapist's profile links.
     *
     * @param $crawler
     *   Crawler object.
     *
     * @return array
     *   Array that contains therapist's profile URLs.
     */
    public function getTherapistsProfileUrls($crawler) {
        $therapistProfileUrls = $crawler->filter($this->therapistProfileCssSelector)->extract(array('href'));

        return $therapistProfileUrls;
    }

    /**
     * Extract therapist's website link.
     *
     * @param $crawler
     *   Crawler object.
     *
     * @return string
     *   String that contains therapist's website URL.
     */
    public function getTherapistsWebsiteUrl($crawler) {
        // If there is button to go to therapist's website.
        if ($crawler->filter("a.btn.btn-block.hidden-xs")->count() > 0) {
            $therapistWebsiteLink = $crawler->filter("a.btn.btn-block.hidden-xs")->eq(0)->extract(array('href'));
        } else {
            $therapistWebsiteLink = NULL;
        }

        return $therapistWebsiteLink[0];
    }

    /**
     * Extract therapist's address.
     *
     * @param $crawler
     *   Crawler object.
     *
     * @return array
     *   Array that contains therapist's address.
     */
    public function getTherapistsAddress($crawler) {
        $therapistsAddress = array();

        // If there is main address - gathers that and puts into the array.
        if ($crawler->filter("div.address.address-rank-1")->count() > 0) {
            $address = $crawler->filter("div.address.address-rank-1")->children();
            $address->filter('span')->eq(0)->count() > 0 ? $therapistsAddress['street_address'] = trim($address->filter('span')->eq(0)->text()) : $therapistsAddress['street_address'] = NULL;
            $address->filter('span')->eq(1)->count() > 0 ? $therapistsAddress['city'] = trim($address->filter('span')->eq(1)->text()) : $therapistsAddress['city'] = NULL;
            $address->filter('span')->eq(2)->count() > 0 ? $therapistsAddress['state'] = trim($address->filter('span')->eq(2)->text()) : $therapistsAddress['state'] = NULL;
            $address->filter('span')->eq(3)->count() > 0 ? $therapistsAddress['zip'] = trim($address->filter('span')->eq(3)->text()) : $therapistsAddress['zip'] = NULL;

            // If there is additional address - gathers that and puts into the array.
            if ($crawler->filter("div.address.address-rank-2")->count() > 0) {
                $additionalAddress = $crawler->filter("div.address.address-rank-2")->children();
                $additionalAddress->filter('span')->eq(0)->count() > 0 ? $therapistsAddress['ad_street_address'] = trim($additionalAddress->filter('span')->eq(0)->text()) : $therapistsAddress['ad_street_address'] = NULL;
                $additionalAddress->filter('span')->eq(1)->count() > 0 ? $therapistsAddress['ad_city'] = trim($additionalAddress->filter('span')->eq(1)->text()) : $therapistsAddress['ad_city'] = NULL;
                $additionalAddress->filter('span')->eq(2)->count() > 0 ? $therapistsAddress['ad_state'] = trim($additionalAddress->filter('span')->eq(2)->text()) : $therapistsAddress['ad_state'] = NULL;
                $additionalAddress->filter('span')->eq(3)->count() > 0 ? $therapistsAddress['ad_zip'] = trim($additionalAddress->filter('span')->eq(3)->text()) : $therapistsAddress['ad_zip'] = NULL;
            }
        } else {
            $therapistsAddress = NULL;
        }

        return $therapistsAddress;
    }

    /**
     * Get all Emails from the given text.
     *
     * @param $text
     *   String that contains site's content.
     * @param $pattern
     *   Email regex pattern.
     *
     * @return array
     *   Array of emails.
     */
    public function getEmails($text, $pattern) {
        // Clears the text content and split it by any symbol
        // (except . and @) and whitespace.
        $text = htmlentities($text, NULL, 'utf-8');
        $text = strip_tags($text);
        $text = str_replace(array("&nbsp;", "&lt;", "&amp;"), " ", $text);
        $splittedPageText = preg_split("/[(\?\|\\\,\"\;\:\№\(\)\[\]\<\>)*\s,]+/", $text);
        $splittedPageText = array_map("StrToLower", $splittedPageText);

        // Search for e-mails in splitted text.
        $emails = preg_grep($pattern, $splittedPageText);

        // Creates array of unique email addresses.
        $result = array();
        array_walk_recursive($emails, function ($value, $key) use (&$result) {
            $result[] = $value;
        });
        $emails = $result;
        $emails = array_unique($emails);

        if (!empty($emails)) {
            return $emails;
        } else {
            return NULL;
        }
    }

    /**
     * Finds all link that contain 'contact' text (case insensitive).
     *
     * @param $crawler
     *   Crawler object.
     *
     * @return string
     *   String that contains contact page URL if it exists, else NULL.
     */
    public function getContactUrl($crawler) {
        // Gets all links from main page.
        $contactLinks = $crawler->filter("a")->links();

        // Search for link, that contain word 'contact' have same base URL.
        $links = array();
        foreach ($contactLinks as $link) {
            $menuLink = $link->getNode()->nodeValue;
            if (preg_grep("/^.*\b(contact)\b.*$/i" , array($menuLink))) {
                $pageUri = $crawler->getBaseHref();
                $linkUri = $link->getUri() . '/';
                if (strstr($linkUri, $pageUri)) {
                    $links[] = $link->getUri();
                }
            }
        }
        if (!empty($links)) {
            $links = array_unique($links);
            return $links[0];
        } else {
            return NULL;
        }
    }

    /**
     * Finds all links that are visible on main page
     * of the given site that have same base URL.
     *
     * @param $crawler
     *   Crawler object.
     *
     * @return array
     *   Array of all URLs.
     */
    public function getAllLinksUrl($crawler) {
        // Gets all links from main page.
        $allLinks = $crawler->filter("a")->links();

        // Gets links URLs.
        $absoluteLinks = array();
        foreach ($allLinks as $item) {
            $absoluteLinks[] = $item->getUri();
        }

        // Creates variable that contain site base URL.
        $parsedUrl = parse_url($crawler->getBaseHref());
        $urlHost = $parsedUrl['host'];

        // Search for links, that have same base URL.
        $links = array();
        foreach ($absoluteLinks as $link) {
            if (strstr($link, $urlHost)) {
                $links[] = $link;
            }
        }
        $links = array_unique($links);

        return $links;
    }

    /**
     * Finds pager's URLs.
     *
     * @param $amount
     *   Amount of pages that needs to be returned.
     *
     * @return array
     *   Array of URLs.
     */
    public function findPages($amount) {
        // Creates crawler by requesting given URL.
        $crawler = $this->proxyRequest($this->baseUrl);
        if ($crawler == NULL) {
            return NULL;
        }

        // Gets URL of pager's active page.
        $linkHereBase = $crawler->filter('a.pager-page.here.hidden-xs.hidden-sm')->extract(array('href'));
        $linkHereBase = $this->changeStateAbbreviation($linkHereBase[0]);

        // Creates an array that have base URL as a first item.
        $allLinks = array($linkHereBase);

        $last = FALSE;
        while (!$last) {
            // Creates crawler by requesting last URL in the array.
            $crawler = $this->proxyRequest(end($allLinks));
            if ($crawler == NULL) {
                return NULL;
            }

            // If this is a start array with single element - delete this element
            // to prevent duplicate values.
            if (count($allLinks) == 1) {
                $allLinks = array();
            }

            // Gets unique links from the given page and save them to the array.
            $links = $crawler->filter('a.pager-page.hidden-xs.hidden-sm')->extract(array('href'));
            foreach ($links as $link) {
                if (!in_array($link, $allLinks)) {
                    $link = $this->changeStateAbbreviation($link);
                    $allLinks[] = $link;
                }
            }

            // Checks if first element of the array is base URL, if not -
            // search for that element and delete everything before it.
            if ($allLinks[0] != $linkHereBase) {
                for ($i = 0; $i < count($allLinks); $i++) {
                    if (($allLinks[$i] == $linkHereBase) && ($i != 0)) {
                        array_splice($allLinks, 0, $i);
                        break;
                    }
                }
            }

            // If there are links in the array.
            if (count($allLinks) > 0) {
                // Gets last link from the array.
                $linkLast = end($allLinks);

                // Gets last link from the last pager button.
                $linkNext = $crawler->filter('div.endresults-right.text-right a:last-child')->extract(array('href'));
                $linkNext = $this->changeStateAbbreviation($linkNext[0]);

                // If amount of pages more than 0 and not ALL.
                if (($amount > '0') && ($amount != 'all')) {
                    // If there are enough links.
                    if (count($allLinks) > $amount) {
                        array_splice($allLinks, $amount + 1);
                        $resultLinks = $allLinks;
                        $last = true;
                        break;
                    } elseif ($linkNext == $linkLast) {
                        // If there are no more pages - return everything that
                        // have been scraped.
                        $resultLinks = $allLinks;
                        $last = true;
                        break;
                    }
                } elseif ($amount == 'all') { // If amount of pages is equal ALL.
                    if ($linkNext == $linkLast) {
                        $resultLinks = $allLinks;
                        $last = true;
                        break;
                    }
                } elseif ($amount == '0') { // If amount of pages is equal 0.
                    $resultLinks = array($linkHereBase);
                    $last = true;
                    break;
                }
            } else {
                return NULL;
            }
        }

        return $resultLinks;
    }


    /**
     * Changes US state abbreviation in given string.
     *
     * @param $input
     *   String that needs to be changed.
     *
     * @return mixed
     *   Modified string if there are any changes or original string
     *   if there are no changes have been made.
     */
    public function changeStateAbbreviation($input) {
        $states = array(
            'AL' => 'Alabama',
            'AK' => 'Alaska',
            'AZ' => 'Arizona',
            'AR' => 'Arkansas',
            'CA' => 'California',
            'CO' => 'Colorado',
            'CT' => 'Connecticut',
            'DE' => 'Delaware',
            'FL' => 'Florida',
            'GA' => 'Georgia',
            'HI' => 'Hawaii',
            'ID' => 'Idaho',
            'IL' => 'Illinois',
            'IN' => 'Indiana',
            'IA' => 'Iowa',
            'KS' => 'Kansas',
            'KY' => 'Kentucky',
            'LA' => 'Louisiana',
            'ME' => 'Maine',
            'MD' => 'Maryland',
            'MA' => 'Massachusetts',
            'MI' => 'Michigan',
            'MN' => 'Minnesota',
            'MS' => 'Mississippi',
            'MO' => 'Missouri',
            'MT' => 'Montana',
            'NE' => 'Nebraska',
            'NV' => 'Nevada',
            'NH' => 'New+Hampshire',
            'NJ' => 'New+Jersey',
            'NM' => 'New+Mexico',
            'NY' => 'New+York',
            'NC' => 'North+Carolina',
            'ND' => 'North+Dakota',
            'OH' => 'Ohio',
            'OK' => 'Oklahoma',
            'OR' => 'Oregon',
            'PA' => 'Pennsylvania',
            'RI' => 'Rhode+Island',
            'SC' => 'South+Carolina',
            'SD' => 'South+Dakota',
            'TN' => 'Tennessee',
            'TX' => 'Texas',
            'UT' => 'Utah',
            'VT' => 'Vermont',
            'VA' => 'Virginia',
            'WA' => 'Washington',
            'WV' => 'West+Virginia',
            'WI' => 'Wisconsin',
            'WY' => 'Wyoming',
            'AS' => 'American+Samoa',
            'DC' => 'Disctrict+Of+Columbia',
            'GU' => 'Guam',
            'MP' => 'Northern+Mariana+Islands',
            'PR' => 'Puerto+Rico',
            'VI' => 'United+States+Virgin+Islands',
            'AB' => 'Alberta',
            'BC' => 'British+Columbia',
            'MB' => 'Manitoba',
            'NB' => 'New+Brunswick',
            'NL' => 'Newfoundland+and+Labrador',
            'NS' => 'Nova+Scotia',
            'NT' => 'Northwest+Territories',
            'NU' => 'Nunavut',
            'ON' => 'Ontario',
            'PE' => 'Prince+Edward+Island',
            'QC' => 'Quebec',
            'SK' => 'Saskatchewan',
            'YT' => 'Yukon',
        );
        foreach ($states as $key=>$value) {
          if (stristr($input, $value)) {
            return str_ireplace($value, $key, $input);
          }
        }
        return $input;
    }

    /**
     * Gets proxy from proxicity.com.
     *
     * @return null|string
     *   If there is a proxy - return string ip:port, else returns NULL.
     */
    public function getProxy() {
        $data = json_decode(file_get_contents('https://api.proxicity.io/v2/8b11feda7c488a90ac8a45e37e716a4f13ac6d5fc03e5c664acaa0bbe85742a2/proxy?httpsSupport=true&isAnonymous=true&port=80'), 1);
        if (isset($data['ipPort'])) {
            return $data['ipPort'];
        }

        return NULL;
    }


    /**
     * Makes request via proxy IP.
     *
     * @param $url
     *   URL that needs to be requested.
     *
     * @return null|\Symfony\Component\DomCrawler\Crawler
     *   Returns Crawler object or NULL if there is no result.
     */
    public function proxyRequest($url) {
        $client = new Client();
        if (($this->proxy == '') && ($this->useBaseIp == 'false')) {
            $this->logger->addInfo('Skipping base IP.');
            $this->proxy = $this->getProxy();
            $this->logger->addInfo('New proxy: ' . $this->proxy);
        } elseif (($this->proxy == '') && ($this->useBaseIp == 'true')) {
            $this->logger->addInfo('Using base IP.');
        }
        $guzzleClient = new \GuzzleHttp\Client(array(
            'proxy' => $this->proxy,
            'delay' => $this->requestDelay,
            'curl' => array(
                CURLOPT_TIMEOUT => 15,
            )
        ));
        $client->setClient($guzzleClient);
        $client->setMaxRedirects(5);
        $client->setHeader('User-Agent', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/55.0.2883.87 Safari/537.36');

        $i = 0;
        while ($i <= $this->amountOfProxies) {
            try {
                $crawler = $client->request('GET', $url);
                if ($crawler != NULL) {
                    $responseCode = $client->getResponse()->getStatus();
                    if ($responseCode < 400) {
                        return $crawler;
                    } else {
                        $this->logger->addInfo('HTTP response: ' . $responseCode);
                    }
                    if ($this->amountOfProxies == 0) {
                        return NULL;
                    }
                }
            } catch (Exception $e) {
                $this->logger->addInfo('Exception caught with message: ' . $e->getMessage());
            }

            $oldProxies[] = $this->proxy;
            $this->proxy = $this->getProxy();
            if (in_array($this->proxy, $oldProxies)) {
                $j = 0;
                while ($j <= $this->amountOfProxies) {
                    $this->proxy = $this->getProxy();
                    if (!in_array($this->proxy, $oldProxies)) {
                        break;
                    }
                    $j++;
                }
            }
            $this->logger->addInfo('New proxy: ' . $this->proxy);
            $guzzleClient = new \GuzzleHttp\Client(array(
                'proxy' => $this->proxy,
                'delay' => $this->requestDelay,
                'curl' => array(
                    CURLOPT_TIMEOUT => 15,
                )
            ));
            $client->setClient($guzzleClient);
            $i++;
        }

    }

    /**
     * Checks that URL is accessible and request do not return any errors.
     *
     * @param $url
     *   URL to request to.
     *
     * @return array|bool|string
     *   If URL is accessible - array with 'result' item as TRUE and 'url' item
     *   that contains last accessed URL as a string.
     *   If URL is not accessible - returns FALSE.
     */
    public function curlCheckRedirect($url) {
        $i = 0;
        $result = '';
        while ($i < $this->amountOfProxies) {
            try {
                // Initialize cURL.
                $curl = curl_init($url);
                curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
                curl_setopt($curl, CURLOPT_FORBID_REUSE, true);
                curl_setopt($curl, CURLOPT_FRESH_CONNECT, true);
                curl_setopt($curl, CURLOPT_PROXY, $this->proxy);
                curl_setopt($curl, CURLOPT_FOLLOWLOCATION, true);
                curl_setopt($curl, CURLOPT_MAXREDIRS, 5);
                curl_setopt($curl, CURLOPT_TIMEOUT, 15);
                curl_setopt($curl, CURLOPT_USERAGENT, "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/55.0.2883.87 Safari/537.36");

                // Execute cURL and get an error code and last effective URL.
                $response = curl_exec($curl);
                $errorCode = curl_errno($curl);
                $lastUrl = curl_getinfo($curl, CURLINFO_EFFECTIVE_URL);

                // If last effective URL is not from PsychologyToday or
                // it's redirect service - assign this URL to the variable.
                if ((!strstr($lastUrl, 'therapists.psychologytoday.com')) && (!strstr($lastUrl, 'out.psychologytoday.com'))) {
                    $url = $lastUrl;
                }

                // If there is no errors in response -
                // checks sites content on protection and accessibility.
                if ($errorCode == 0) {
                    $checkContent = $this->checkSitesContent($response);
                    switch ($checkContent['protection']) {
                        case 'WIX':
                            $this->logger->addInfo('Site created using Wix. There is no access to this site.');
                            return FALSE;
                        case 'CF':
                            $this->logger->addInfo('Detected CloudFlare e-mail protection.');
                            return FALSE;
                        case 'INC':
                            $this->logger->addInfo('Detected Incapsula protection. There is no access to this site.');
                            return FALSE;
                        case 'FN':
                            $this->logger->addInfo('Detected Fortinet protection, trying to avoid it with different proxy IP.');
                            break;
                        case 'RC':
                            $this->logger->addInfo('Detected reCAPTCHA, trying to avoid it with different proxy IP.');
                            break;
                    }
                }

                // If there is no errors and content is OK - return TRUE and
                // last effective URL in an array.
                if (($errorCode == 0) && ($checkContent === TRUE)) {
                    $result = array(
                        'result' => true,
                        'url' => $lastUrl,
                    );

                    return $result;
                } else { // Else - get a new proxy and repeat request.
                    $oldProxies[] = $this->proxy;
                    $this->proxy = $this->getProxy();
                    if (in_array($this->proxy, $oldProxies)) {
                        $j = 0;
                        while ($j <= $this->amountOfProxies) {
                            $this->proxy = $this->getProxy();
                            if (!in_array($this->proxy, $oldProxies)) {
                                break;
                            }
                            $j++;
                        }
                    }
                    $this->logger->addInfo('New proxy: '.$this->proxy);
                    $i++;
                }
            } catch (Exception $e) {
                $this->logger->addInfo('Exception caught with message: ' . $e->getMessage());
            }
        }

        // If there is no result - return FALSE.
        if ($result == '') {
            return FALSE;
        } else {
            return $result;
        }
    }

    /**
     * Checks sites content if any protection used (Wix, CloudFlare,
     * Fortinet, reCAPTCHA, Incapsula).
     *
     * @param $sitesHtml
     *   Sites HTML as a string.
     *
     * @return array|bool
     *   TRUE if there are no protection, otherwise array with two keys -
     *   "result => FALSE" and "protection" which contains name of protection.
     */
    public function checkSitesContent($sitesHtml) {
        if ((stristr($sitesHtml, 'BEAT MESSAGE')) || (strstr($sitesHtml, 'static.wixstatic.com'))) {
            $result = array(
                'result' => FALSE,
                'protection' => 'WIX',
            );
            return $result;
        }

        if ((stristr($sitesHtml, '__cf_email__')) || (strstr($sitesHtml, 'email-protection'))) {
            $result = array(
                'result' => FALSE,
                'protection' => 'CF',
            );
            return $result;
        }

        if ((stristr($sitesHtml, 'FortiGate')) || (strstr($sitesHtml, 'Fortinet'))) {
            $result = array(
                'result' => FALSE,
                'protection' => 'FN',
            );
            return $result;
        }

        if ((stristr($sitesHtml, '<iframe src="https://www.google.com/recaptcha/api/')) && (strstr($sitesHtml, 'why_captcha_headline'))) {
            $result = array(
                'result' => FALSE,
                'protection' => 'RC',
            );
            return $result;
        }

        if (stristr($sitesHtml, 'Incapsula')) {
            $result = array(
                'result' => FALSE,
                'protection' => 'INC',
            );
            return $result;
        }

        return TRUE;
    }

    /**
     * Search e-mails on every given site.
     */
    public function searchForEmails() {
        // Creates new Goutte client.
        $client = new Client();

        // Create new Guzzle client with request delay and timeout value.
        $guzzleClient = new \GuzzleHttp\Client(array(
            'delay' => $this->requestDelay,
            'curl' => array(
                CURLOPT_CONNECTTIMEOUT => 15,
            )
        ));

        // Sets new Guzzle client to the Goutte client.
        $client->setClient($guzzleClient);
        $client->setMaxRedirects(5);
        $client->setHeader('User-Agent', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/55.0.2883.87 Safari/537.36');

        // Search for e-mails for every therapist.
        for ($i = 0; $i < count($this->therapistNames); $i++) {
            try {
                $this->logger->addInfo('Searching for emails for ' . $this->therapistNames[$i] . ' - ' . ($i + 1) . '/' . count($this->therapistNames));
                // If there is link to therapist's website.
                if ($this->therapistWebsiteUrls[$i] != NULL) {
                    // Checks that URL is accessible and don't returns error codes.
                    $this->logger->addInfo('Checking that URL is accessible...');
                    $result = $this->curlCheckRedirect($this->therapistWebsiteUrls[$i]);

                    // If URL is accessible.
                    if ($result['result'] == TRUE) {
                        $this->therapistWebsiteUrls[$i] = $result['url'];
                        // Creates new crawler by requesting therapist's website.
                        $crawler = $this->proxyRequest($this->therapistWebsiteUrls[$i]);
                        if ($crawler != NULL) {
                            // Gets site's HTML.
                            $sitesHtml = $this->getSitesHtml($crawler);
                            // Search for e-mails on the site.
                            $this->allEmails[$i] = $this->getEmails($sitesHtml, $this->emailPattern);

                            // If there is no e-mails on the main page - find link to contact page.
                            if (empty($this->allEmails[$i])) {
                                $contactUrl = $this->getContactUrl($crawler);
                                if ($contactUrl != NULL) {
                                    // If there is contact page - search there for e-mails.
                                    $contactPageCrawler = $this->proxyRequest($contactUrl);
                                    $contactPageHtml = $this->getSitesHtml($contactPageCrawler);
                                    $email = $this->getEmails($contactPageHtml, $this->emailPattern);
                                    if (($email != NULL) && (!empty(current($email)))) {
                                        $this->allEmails[$i] = $email;
                                    } else {
                                        $this->logger->addInfo('No emails found on contact page.');
                                        $this->allEmails[$i] = NULL;
                                    }
                                } else {
                                    $this->logger->addInfo('No contact page found.');
                                }

                                // If there is no e-mails on the contact page - find every link that is visible from main page.
                                if (empty($this->allEmails[$i])) {
                                    $allLinks = $this->getAllLinksUrl($crawler);
                                    if (!empty($allLinks)) {
                                        // If there are any pages - search there for e-mails.
                                        foreach ($allLinks as $item) {
                                            $allLinksCrawler = $this->proxyRequest($item);
                                            $pagesHtml = $this->getSitesHtml($allLinksCrawler);
                                            $email = $this->getEmails($pagesHtml, $this->emailPattern);
                                            if (($email != NULL) && (!empty(current($email)))) {
                                                $this->allEmails[$i] = $email;
                                                break;
                                            } else {
                                                $this->allEmails[$i] = NULL;
                                            }
                                        }
                                        if (empty($this->allEmails[$i])) {
                                            $this->logger->addInfo('No emails found.');
                                        }
                                    } else {
                                        $this->logger->addInfo('Can not get any links.');
                                        $this->allEmails[$i] = NULL;
                                    }
                                }
                            }
                        } else {
                            $this->logger->addInfo('Empty response.');
                            $this->allEmails[$i] = NULL;
                        }
                    } else {
                        $this->logger->addInfo('URL is not accessible.');
                        $this->allEmails[$i] = NULL;
                    }
                } else {
                    $this->logger->addInfo('No website link.');
                    $this->allEmails[$i] = NULL;
                }
                $this->writeToCSV($i);
            } catch (Exception $e) {
                $this->logger->addInfo('Exception caught with message: ' . $e->getMessage());
            }
        }
    }

    /**
     * Main function that gets all the information about therapists
     * and parsing their sites for emails.
     *
     * @param $urls
     *   Array of URLs to search from.
     *
     * @return
     *   TRUE if there are any results, NULL if there are not.
     */
    public function main($urls = NULL) {
        // If there aren't URLs given.
        if ($urls == NULL) {
            // Find needed amount of pages to scrape from.
            $this->logger->addInfo('Finding pages...');
            $pages = $this->findPages($this->amountOfPages);
            // If there are any pages.
            if ($pages != NULL) {
                // Gets basic information for every therapist on each page.
                foreach ($pages as $key=>$page) {
                    try {
                        $this->logger->addInfo('Scraping page ' . ($key + 1) . '/' . count($pages));
                        // Creates new crawler by requesting each page.
                        $crawler = $this->proxyRequest($page);
                        if ($crawler != NULL) {
                            // Gets all therapists' profile links.
                            $therapistProfileUrls = $this->getTherapistsProfileUrls($crawler);

                            // For each link gets therapist's name,
                            // website URL and addresses.
                            foreach ($therapistProfileUrls as $keys => $item) {
                                $this->logger->addInfo('Scraping therapists profile ' . $item);
                                $crawler = $this->proxyRequest($item);
                                if ($crawler != NULL) {
                                    $this->therapistNames[] = $this->getTherapistsName($crawler);
                                    $this->therapistProfileUrls[] = $item;
                                    $this->therapistWebsiteUrls[] = $this->getTherapistsWebsiteUrl($crawler);
                                    $this->therapistAddresses[] = $this->getTherapistsAddress($crawler);
                                }
                            }
                        }
                    } catch (Exception $e) {
                        $this->logger->addInfo('Exception caught with message: ' . $e->getMessage());
                    }
                }

                if (empty($this->therapistWebsiteUrls)) {
                    return NULL;
                }

                // Search for e-mails for every therapist.
                $this->searchForEmails();
            } else {
                $this->logger->addInfo('No pages found.');
                return NULL;
            }
        } else {
            // Therapist's profile URLs is equal to given URLs.
            $this->therapistProfileUrls = $urls;
            for ($i = 0; $i < count($urls); $i++) {
                if (strstr($urls[$i], 'http://www.aamft.org/')) { //If URL from aamft.org.
                    $this->logger->addInfo('Scraping therapists profile ' . $urls[$i]);
                    $crawler = $this->proxyRequest($urls[$i]);
                    // On given page search for table with e-mail.
                    if ($crawler->filter("font > table")->count() > 0) {
                        $text = $crawler->filter('font > table');
                        $infoTable = $text->filter('td')->last()->text();
                        $splittedText = preg_split('/\s/', $infoTable);
                        $this->therapistWebsiteUrls[$i] = preg_grep('/(.*)\.(.*)/', $splittedText);

                        // If there is no protocol in the website URL - add HTTP protocol to URL.
                        $url = $this->therapistWebsiteUrls[$i][0];
                        if (!empty($url)) {
                            if (!preg_match("~^(?:f|ht)tps?://~i", $url)) {
                                $this->therapistWebsiteUrls[$i] = 'http://' . $url;
                            }
                        }

                        // Search for e-mail in information table.
                        $emailsText = $crawler->filter('table.twTable')->text();
                        $email = $this->getEmails(array($emailsText), $this->emailPattern);

                        // If there are any e-mails on the profile page.
                        if (!empty($email)) {
                            $result = array();
                            array_walk_recursive($email, function ($value, $key) use (&$result) {
                                $result[] = $value;
                            });
                            $this->allEmails[] = $result;
                        }
                    } else {
                        $this->therapistWebsiteUrls[$i] = NULL;
                    }
                } elseif (strstr($urls[$i], 'https://therapists.psychologytoday.com/')) { //If URL from therapists.psychologytoday.com.
                    $this->logger->addInfo('Scraping therapists profile ' . $urls[$i]);
                    $crawler = $this->proxyRequest($urls[$i]);
                    if ($crawler != NULL) {
                        $this->therapistNames[] = $this->getTherapistsName($crawler);
                        $this->therapistWebsiteUrls[] = $this->getTherapistsWebsiteUrl($crawler);
                        $this->therapistAddresses[] = $this->getTherapistsAddress($crawler);
                    }
                }
            }

            // If there are no website URLs - return NULL.
            if (empty($this->therapistWebsiteUrls)) {
                return NULL;
            }

            // Search for e-mails for every therapist.
            $this->searchForEmails();
        }

        // Creates array of unique e-mails.
        for ($i = 0; $i < count($this->allEmails); $i++) {
            if ($this->allEmails[$i] != NULL) {
                $this->allEmails[$i] = array_filter($this->allEmails[$i]);
                $this->allEmails[$i] = array_unique($this->allEmails[$i]);
            }
        }

        if (!empty($this->therapistProfileUrls)) {
            return TRUE;
        } else {
            return NULL;
        }
    }

    /**
     * Prints all results on the page.
     *
     * @return
     *   Response object if there is result to display, NULL if results are empty.
     */
    public function printResults() {
        // Creates array with information for every therapist.
        $output = array();
        if (!empty($this->therapistProfileUrls)) {
            for ($i = 0; $i < count($this->therapistProfileUrls); $i++) {
                // If there is therapist's name - creates link with name to profile.
                // Else creates link to profile without name.
                if (!empty($this->therapistNames)) {
                    $output[$i]['therapistName'] = $this->therapistNames[$i];
                    $output[$i]['therapistProfileUrl'] = $this->therapistProfileUrls[$i];
                    if ($this->therapistWebsiteUrls[$i] != NULL) {
                        $output[$i]['therapistWebsiteUrl'] = $this->therapistWebsiteUrls[$i];
                    } else {
                        $output[$i]['therapistWebsiteUrl'] = NULL;
                    }
                } else {
                    $output[$i]['therapistProfileUrl'] = $this->therapistProfileUrls[$i];
                    if (!empty($this->therapistWebsiteUrls[$i])) {
                        $output[$i]['therapistWebsiteUrl'] = $this->therapistWebsiteUrls[$i];
                    }
                    else {
                        $output[$i]['therapistWebsiteUrl'] = NULL;
                    }
                }

                // Make sure that every empty e-mail record is NULL.
                if ($this->allEmails[$i] != NULL) {
                    if ((array_key_exists('0', $this->allEmails[$i])) && ((count($this->allEmails[$i][0]) == 0))) {
                        $this->allEmails[$i] = NULL;
                    }
                }

                // Output e-mails.
                if (($this->allEmails[$i] != NULL)) {
                    $result = array();
                    array_walk_recursive($this->allEmails[$i], function ($value, $key) use (&$result) {
                        $result[] = $value;
                    });
                    foreach ($result as $value) {
                        $output[$i]['therapistEmails'][] = $value;
                    }
                } else {
                    $output[$i]['therapistEmails'] = NULL;
                }

                // Output address and additional address if exist.
                if (!empty($this->therapistNames)) {
                    if (($this->therapistAddresses[$i] != NULL)) {
                        $output[$i]['therapistAddresses'][] = "Main address:";
                        $this->therapistAddresses[$i]['street_address'] != NULL ? $output[$i]['therapistAddresses'][] = 'Street: ' . $this->therapistAddresses[$i]['street_address'] : NULL;
                        $this->therapistAddresses[$i]['city'] != NULL ? $output[$i]['therapistAddresses'][] = 'City: ' . $this->therapistAddresses[$i]['city'] : NULL;
                        $this->therapistAddresses[$i]['state'] != NULL ? $output[$i]['therapistAddresses'][] = 'State: ' . $this->therapistAddresses[$i]['state'] : NULL;
                        $this->therapistAddresses[$i]['zip'] != NULL ? $output[$i]['therapistAddresses'][] = 'ZIP: ' . $this->therapistAddresses[$i]['zip'] : NULL;
                        if (array_key_exists('ad_street_address', $this->therapistAddresses[$i])) {
                            $output[$i]['therapistAddresses'][] = "Additional address:";
                            $this->therapistAddresses[$i]['ad_street_address'] != NULL ? $output[$i]['therapistAddresses'][] = 'Street: ' . $this->therapistAddresses[$i]['ad_street_address'] : NULL;
                            $this->therapistAddresses[$i]['ad_city'] != NULL ? $output[$i]['therapistAddresses'][] = 'City: ' . $this->therapistAddresses[$i]['ad_city'] : NULL;
                            $this->therapistAddresses[$i]['ad_state'] != NULL ? $output[$i]['therapistAddresses'][] = 'State: ' . $this->therapistAddresses[$i]['ad_state'] : NULL;
                            $this->therapistAddresses[$i]['ad_zip'] != NULL ? $output[$i]['therapistAddresses'][] = 'ZIP: ' . $this->therapistAddresses[$i]['ad_zip'] : NULL;
                        }
                    } else {
                        $output[$i]['therapistAddresses'][] = NULL;
                    }
                }
            }

            // Render template with all the info.
            $response = $this->render("Main/result-table.html.twig", array('output' => $output));
            echo $response->getContent();
        } else {
            return NULL;
        }
    }

    /**
     * Writes a line with therapist's information into CSV file.
     *
     * @param $i
     *   Index of the therapist in $this variable.
     */
    public function writeToCSV($i) {

        $file = fopen('emails.csv', 'a');

        $flagSites = FALSE;
        $flagEmails = FALSE;
        $outputSites = array();
        $outputEmails = array();
        $outputAddress = array();
        $resultSites = array();
        $resultEmails = array();
        $field = array();

        // Creates CSV array of website URLs.
        if (is_array($this->therapistWebsiteUrls[$i])) {
            foreach ($this->therapistWebsiteUrls[$i] as $key => $item) {
                if ($key == 0) {
                    $outputSites[$i] = $item;
                } else {
                    $outputSites[$i] .= "," . $item;
                }
            }
            $flagSites = TRUE;
        }

        // Creates CSV array of e-mails.
        if (is_array($this->allEmails[$i])) {
            $result = array();
            array_walk_recursive($this->allEmails[$i], function ($item, $key)  use (&$result) {
                $result[] = $item;
            });
            $result = array_unique($result);
            foreach ($result as $key => $value) {
                if ($key == 0) {
                    $outputEmails[$i] = $value;
                } else {
                    $outputEmails[$i] .= "," . $value;
                }
            }
            $flagEmails = TRUE;
        }

        // Creates CSV array of addresses.
        if (($this->therapistAddresses[$i] != NULL)) {
            foreach ($this->therapistAddresses[$i] as $key => $value) {
                if ($key == 'street_address') {
                    $outputAddress[$i] = $value;
                } else {
                    $outputAddress[$i] .= "," . $value;
                }
            }
        }

        // If there are more than one website - output array, else output website.
        if ($flagSites) {
            $resultSites[$i] = $outputSites[$i];
        } else {
            $resultSites[$i] = $this->therapistWebsiteUrls[$i];
        }

        // If there are more than one e-mail - output array, else output e-mail.
        if ($flagEmails) {
            $resultEmails[$i] = $outputEmails[$i];
        } else {
            $resultEmails[$i] = $this->allEmails[$i];
        }

        // If there are therapists' names - output them as first column and add addresses as a last column.
        if (!empty($this->therapistNames)) {
            $field[$i] = array($this->therapistNames[$i], $resultSites[$i], $resultEmails[$i], $outputAddress[$i]);
        } else {
            $field[$i] = array($this->therapistProfileUrls[$i], $resultSites[$i], $resultEmails[$i]);
        }

        // Writes information into the file.
        foreach ($field as $item) {
            fputcsv($file, $item);
        }

        fclose($file);
    }

    /**
     * Print message via Twig template.
     *
     * @param $message
     *   Message to display.
     *
     * @return Response
     *   Response object.
     */
    public function printMessage($message) {
        return $this->render("Main/output-message.html.twig", array('message' => $message));
    }
}
