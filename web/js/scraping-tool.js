$(document).ready(function(){
  
  drop_area = document.getElementById('url');
  var file = null;
  var delay = 0;
  var proxies = 0;
  var baseIp = 0;
  var amount = 0;
  
  drop_area.ondragover = function () {
    this.className = 'hover';
    return false;
  };
  
  drop_area.ondragleave = function () {
    this.className = '';
    return false;
  };
  
  drop_area.ondragend = function () {
    this.className = '';
    return false;
  };
  
  drop_area.ondrop = function (e) {
    this.className = '';
    e.preventDefault();
    
    file = e.dataTransfer.files[0];
    if (file.type == 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet') {
      $("#url").val(file.name);
      $("#amount").prop('disabled', true);
      $("#url").prop('disabled', true);
    } else {
      file = null;
      alert('File should have .xlsx extension!');
      $("#url").val('');
    }
  };
  
  $("#submit-button").click(function(){
    
    if ($('#url').val() != '') {
      if ((file != null) && ($("#url").val() == file.name)) {
        var formData = new FormData();
        delay = $("#delay").val();
        proxies = $("#proxies").val();
        baseIp = document.getElementById('base-ip').checked;
        formData.append('file-upload', file);
        formData.append('delay', delay);
        formData.append('proxies', proxies);
        formData.append('baseIp', baseIp);
    
        $.ajax({
          type: 'POST',
          url: "/",
          data: formData,
          processData: false,
          contentType: false,
          beforeSend: function (result) {
            $(".button-loader").show();
            $(".button-label").hide();
            $("#result-area").hide();
          },
          success: function (result) {
            $(".button-loader").hide();
            $(".button-label").show();
            $("#result-area").show();
            $("#result-area").html(result);
            $("#amount").prop('disabled', false);
          },
          error: function (request, status, error) {
            alert('Something went wrong...' + request.responseText);
            $(".button-loader").hide();
            $(".button-label").show();
          }
        });
      }
      else {
        var url = $("#url").val();
        if (url.indexOf('https://therapists.psychologytoday.com') != -1) {
          delay = $("#delay").val();
          if ($("#amount").prop('disabled') == false) {
            amount = $("#amount").val();
            if (!(((Math.floor(amount)) || (amount == 0)) && (amount >= 0) && (amount <= 500))) {
              alert('Amount of pages should be an integer between 0 and 500');
              return false;
            }
          } else {
            amount = 'all';
          }
          proxies = $("#proxies").val();
          baseIp = document.getElementById('base-ip').checked;
          var data = {url: url, delay: delay, amount: amount, proxies: proxies, baseIp: baseIp};
          
          $.ajax({
            type: 'POST',
            url: "/",
            data: data,
            dataType: 'text',
            beforeSend: function (result) {
              $(".button-loader").show();
              $(".button-label").hide();
              $("#result-area").hide();
            },
            success: function (result) {
              $(".button-loader").hide();
              $(".button-label").show();
              $("#result-area").show();
              $("#result-area").html(result);
            },
            error: function (request, status, error) {
              alert('Something went wrong...' + request.responseText);
              $(".button-loader").hide();
              $(".button-label").show();
            }
          });
        }
        else {
          alert('Link should be from https://therapists.psychologytoday.com/')
        }
      }
    } else {
      alert("Insert field shouldn't be empty!");
    }
  });
  
  $(".erase-button").click(function() {
    $('#url').val('');
    file = null;
    $("#amount").prop('disabled', false);
    $("#url").prop('disabled', false);
  });
  
  $("#all-pages").click(function(){
    if ($("#amount").prop('disabled') == false) {
      $("#amount").prop('disabled', true);
    } else {
      $("#amount").prop('disabled', false);
    }
  });
  
});
