#Scraping tool


Scrape therapist's information from [Therapist Psychology Today](https://therapists.psychologytoday.com/rms) and [AAMFT](http://www.aamft.org/)

##Requirements

Script depends on PHP 5.5+.

##Installation
* Place the script in a public directory.
* Use Terminal and cd into that directory - "cd scraping-tool".
* Use Symfony built-in web server to launch script in your local environment  - "bin/console server:start".
* In your web browser go to "http://127.0.0.1:8001/" and you should see script's main page.
* To track progress of the execution you can type in this command in your Terminal - "tail -f var/logs/scraper.log" and you would be able to see logs in real-time. Also, you can check the logs in "var/logs/scraper.log".
 
##Available settings
* Delay - delay between requests in seconds/minutes. If value is 0 - no delay will be added.
* Amount of pages - amount of pages to scan additionally. If value is 0 - only source page will be scanned.
* Amount of proxies - amount of proxies that will be used until requested site respond. If value is 0 - no proxy will be used.
 
##Usage
* Copy link to therapists list from https://therapists.psychologytoday.com/rms, for example https://therapists.psychologytoday.com/rms/state/CO/Broomfield.html and insert it to input field OR drag and drop .xlsx file, that contains therapist's profiles links from http://www.aamft.org/, for example http://www.aamft.org/cgi-shl/TWServer.exe?Run:LOCATEOV_2:TradeWinds_KEY=1.
* Set delay, amount of additional pages and amount of proxies settings as you want or leave it in the default state.
* Click on "Find E-mails" button and wait until it's complete.
* After that done on the page you should see table with results and CSV file called "emails.csv" in "web" folder of your script installation directory.
